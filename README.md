A static jekyll site for calyx.net

## Directory layout

* pages: The source content for the site.
* public: Rendered static pages. These are not committed to this repo.

## Building the static pages

Prerequisites:

    sudo apt install git ruby bundler build-essential libxml2 libxslt1.1 zlib1g-dev

Clone git repo:

    git clone https:0xacab.org/calyx/websites/calyx.net.git -b main

Install gems

    cd calyx.net
    bundle config set --local path 'vendor'
    bundle

Build the static pages:

    bundle exec jekyll build

To view the pages via a local server:

    bundle exec jekyll server

## CalyxVPN bits

CalyxVPN clients needs these endpoints:

* https://calyx.net/provider.json
* https://calyx.net/ca.crt

Technically, most clients should have these hardcoded. Those files should be at those locations for Bitmask client to work.

For provider.json: `scp root@app01-nyc:/etc/leap/config/vpnweb/provider.json .`

For ca.cert: `wget https://api.vpn.calyx.net/ca.crt`

## TODO

Currently, we just include a copy of the bootstrap css. It would be nice to import the bootstrap gem,
and import the sass files directly.

Then configure in `_config.yml`:

    sass:
      load_paths:
        - _sass
        - vendor/bundle/ruby/2.5.0/gems/bootstrap-5.1.3/assets/stylesheets/

There are a bunch of problems with this. It makes the `_config.yml` be dependent on the ruby version,
and the gem `bootstrap` includes a ton of unneeded dependencies.

It would be nice to have a jekyll plugin that just altered the sass load_paths for you, so that you didn't have to hard code the deployment specific path.

It would also be nice to have a bootstrap gem that just included the static sass files, and not all the gem dependencies.
